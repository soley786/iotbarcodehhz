**[zurück zu Home](Home)**

# Brainstorming #
Das Ergebnis eines ersten Ideenaustausch war eine Unterstützung des Arbeitsablaufs am HHZ.

![IMG_6499.JPG](https://bitbucket.org/repo/gLkBzg/images/4138703416-IMG_6499.JPG)

Im Nachgang wurden die Service-Ideen in einer Mind-Map festgehalten:

![HHZ Service Living Lab_Brainstorming_V4.jpeg](https://bitbucket.org/repo/gLkBzg/images/3988898471-HHZ%20Service%20Living%20Lab_Brainstorming_V4.jpeg)

Darüberhinaus wurde im Brainstorming festgelegt, wie die Dokumentation im Wiki aufgebaut und sinnvoll gegliedert werden kann:
![IMG_6792.JPG](https://bitbucket.org/repo/gLkBzg/images/2139011310-IMG_6792.JPG)

Aus diesen Brainstorming ist die Idee entstanden, die Dokumentation anhand des [Living Lab - Prozess](Living Lab HHZ) anzulehnen.

Weiterhin haben wir in der Brainstorming-Phase grob den möglichen Ablauf der Services beschrieben und dies besprochen:
![IMG_6794.JPG](https://bitbucket.org/repo/gLkBzg/images/4247133004-IMG_6794.JPG)
