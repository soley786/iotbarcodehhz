#####[Zurück zu Home](Home)  

####Aufbau der Grundlagen
[IBM Bluemix](#IBM Bluemix)  
[Rasberry Pi](#Rasberry Pi)  
[Alexa](#Alexa)  
[Bitbucket](#Bitbucket) 


##IBM Bluemix
Für die Implementierung der Backend-Prozesse verwenden wir in diesem Projekt die PaaS-Cloud Bluemix von IBM. Hierfür haben wir freie Studentenaccounts von IBM zur Verfügung gestellt bekommen (Vielen Dank an dieser Stelle :)). 

Bluemix bietet den Entwicklern einen Zugriff auf mehr als 100 Cloud-Services, um schnell und einfach Webanwendungen und mobile Apps zu entwickeln. Hierfür bietet Bluemix mehrere Services an, die einfach zur implementierten Lösung dazugeklickt werden können.

(Von [Wikipedia](https://de.wikipedia.org/wiki/Bluemix))

##Rasberry Pi
Rasberry Pi ist ein Einplatinencomputer in der Größe einer Kreditkarten. Dieser wurde entwickelt um jungen Leuten spielerisch Programmierkenntnisse beizubringen. Er ist ausgestattet mit einem Ein-Chip-System von Broadcom mit einem ARM-Mikroprozessor. Aufgrund dieser Eigenschaften eignet er sich perfekt als Hardware für den Alexa Sprachassistenten. 

(Von [Wikipedia](https://de.wikipedia.org/wiki/Raspberry_Pi))

##Alexa Voice Service
Alexa Voice Service (AVS) ist ein von Amazon entwickelter Spracherkennungsdienst. Mit der Eingabe natürlicher Sprache über ein Mikrophon extrahiert AVS aus dieser relevante Befehle und führt diese aus. Hierfür ist ein Client auf einem Endgerät (in unserem Fall ein Rasberry Pi) zu installieren der mit den angebunden Amazon-Services kommuniziert. So bietet der AVS dem Benutzer Mehrwert durch Services ohne GUI.

(Von [Amazon Developer Page](https://developer.amazon.com/public/solutions/alexa/alexa-voice-service))

**Für die Vorstellung von Alexa haben wir ein Video erstellt, dass wir sowohl auf [YouTube](https://youtu.be/VtW_hGvNVwE) bereitgestellt, als auch im Download-Bereich hinterlegt haben. 
Über nachfolgenden Link kann das Video heruntergeladen werden kann.
[Alexa Introduction-Video](https://bitbucket.org/iotalexahhz/iotalexahhz/downloads/Alexa_Introduction.mp4)**

##Bitbucket
Bitbucket bietet als SaaS ein Dateiablagesystem an. Hierbei kommen die Versionsverwaltungssysteme Git und Mercurial zum Einsatz. Zusätzlich zur übergreifenden Kollaboration an einem Entwicklungsprojekt bietet Bitbucket ein Wiki zur gemeinsamen Dokumentation der Arbeit. Wir nutzen in unserem Projekt dieses Wiki für die Dokumentation der Arbeitsschritte und wollen so Transparenz schaffen. 

(Von [Wikipedia](https://de.wikipedia.org/wiki/Bitbucket))