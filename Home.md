#Alexa Sprachassisstent

**Herzlich willkommen im Online-Wiki des Projektteams "Alexa Sprachassisstent".** 

Das Projektteam besteht aus nachfolgenden Personen

* Martin Brümmer
* Sascha Jörg
* Max Kienle
* Tobias Lindner
* Sebastian Mantsch
* Johan Zhai

Das Online-Wiki ist wie folgt aufgebaut:
#### Agenda
1. [Einleitung](Einleitung)  
    1. [Vorstellung Anwendungsprojekt](Einleitung) 
    2. [Vorgehen & Gruppe](Einleitung)
    3. [Daily Report](Einleitung)
2. [Theoretische Grundlagen](Theoretische Grundlagen)
    1. [IBM Bluemix](Theoretische Grundlagen) 
    2. [Raspberry Pi](Theoretische Grundlagen)
    3. [Alexa](Theoretische Grundlagen)
    4. [Bitbucket](Theoretische Grundlagen)
3. [Living Lab HHZ](Living Lab HHZ)
    1. [Co-Creation](Co-Creation) 
    2. [Exploration](Exploration)
        1. [Brainstorming](Brainstorming)
        2. [Service-Entwurf](Service Design)
        3. [MVP - bestimmen](MVP)
        4. [Weitere Services bestimmen](Weitere Services - bestimmen)
    3. [Experimentation](Experimentation)
        1. [IBM Bluemix](Experimentation)
            1. [Registrierung fehlgeschlagen](IBM Bluemix - erste Schritte)
            2. [Registrierung erfolgreich](IBM Bluemix - Registrierung)
        2. [Raspberry Pi - Installation & Konfiguration](Raspberry Pi)
        3. [Alexa lebt!](Alexa lebt!)
        4. [MVP - implementieren](MVP implementieren)
        5. [Weitere Services implementieren](Weitere Services - implementieren)
    4. [Evaluation](Evaluation)

Über die Einzelnen Kapitel der obersten Ebene, gelangen Sie zu den einzelnen Übersichtsseiten, die den Themenkomplex kurz eräutern und die Links zu den einzelnen Dokumentationen erhalten. Über die Unterkategorien kommen Sie direkt zu den spezifischen Dokumentationen.

Viel Spass beim Lesen!