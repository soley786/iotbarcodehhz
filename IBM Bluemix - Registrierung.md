**[Zurück zu Home](Home)**

Hier befindet sich die Dokumentation zur Registrierung bei IBM Bluemix.

Register https://ibm.biz/bluemixaicloudoffer for an IBM Bluemix trial account if you haven't already. You will be asked to validate your email address, so please check your in-box and click that validation link. 

Account erstellen:

Ausgefüllte Anmeldungsmaske:
![Bluemix7.png](https://bitbucket.org/repo/gLkBzg/images/142198301-Bluemix7.png)

![Bluemix11.png](https://bitbucket.org/repo/gLkBzg/images/263102191-Bluemix11.png)

Bluemix-Bestätigungsmail:

![bluemix15.png](https://bitbucket.org/repo/gLkBzg/images/201651756-bluemix15.png)

Log https://console.ng.bluemix.net/ in to your IBM Bluemix dashboard, click the number of days remaining on your account in the upper right corner, and click promo code. 


Login von Bluemix:
![bluemixlogin.png](https://bitbucket.org/repo/gLkBzg/images/2518250189-bluemixlogin.png)

Dashboard von Bluemix:
![DashboardBluemix.png](https://bitbucket.org/repo/gLkBzg/images/657686754-DashboardBluemix.png)

Bereich in Bluemix anlegen:
![bereich_bluemix.png](https://bitbucket.org/repo/gLkBzg/images/2936775058-bereich_bluemix.png)

![bereich_bluemix2.png](https://bitbucket.org/repo/gLkBzg/images/3577651284-bereich_bluemix2.png)

Cut and paste the promo code above to the "Enter promo code" field, and click APPLY. Refresh the page to see the extension reflected in the calendar icon. 

![Bluemix22.png](https://bitbucket.org/repo/gLkBzg/images/2723806892-Bluemix22.png)

**Erfolgreich**