**[Zurück zu Home](Home)**

Auf Basis des [implementierten MVPs](MVP implementieren) wurde ein weiterer Service umgesetzt. Dieser bietet den Studenten am HHZ über den Messenger "Telegram" die Möglichkeit, eine Information über ihre Verspätung zu geben. Diese Verspätungsmeldungen bieten den Professoren die Möglichkeit bei Beginn der Vorlesung bei Alexa anzufragen, ob alle Studenten bereits anwesend sind oder ob es angemeldete Verspätungen gibt. Die Systemlandschaft dieses Service sieht folgendermaßen aus:

![Alexa_Systemlandschaft_Telegram.png](https://bitbucket.org/repo/gLkBzg/images/2792570094-Alexa_Systemlandschaft_Telegram.png) 

Der Messenger [Telegram](https://telegram.org/) bietet die Möglichkeit, dass die User Bots erstellen können. Diese Bots können Nachrichten annehmen und versenden. Die Nachrichten werden in der Telegram Datenbank gespeichert und können durch den Botersteller abgerufen werden. Hierfür wird ein Authentifizierungstoken benötigt. Die Anlage eines Bots erfolgt durch das Senden einer Nachricht mit dem Namen des Bots an den Erstellungsbot "botfather". Dieser wird legt dann den eigenen Bot an und gibt den Authentifizierungstoken zurück. Für unseren Use Case haben wir den Bot "HHZ - Verspätung" angelegt.

Bei einer möglichen Verspätung des Studenten kann dieser an den "HHZ - Verspätung"-Bot eine Nachricht mit dem Titel der Vorlesung schreiben. Diese werden dann gespeichert. 

Bei einer Anfrage über den Alexa Sprachassistenten werden die aktuell gespeicherten Nachrichten über Bluemix vom Telegram Server abgerufen. Diese werden dann im Bluemix Node-RED ausgewertet und an den Alexa Sprachassistenten wieder konsolidiert zurückgegeben. Mögliche Ausprägungen dieser Rückgabe sind: Kein Student hat eine Verspätung angemeldet oder 1-n Studenten haben eine Verspätung angemeldet. Je nach Ausprägung gibt Alexa eine andere Sprachdatei inklusive der Anzahl der verspäteten Studenten.

Die Demo für diesen Service ist über [YouTube](https://youtu.be/yGeqXQT_xg0) abrufbar.