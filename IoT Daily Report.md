[Zurück zur Übersicht](https://bitbucket.org/iotalexahhz/iotalexahhz/wiki/Home)
# 24.05.2016 - Team- und Projektzielfindung 

Unboxing der notwendigen Hardware, Festlegen der benötigten Teile (Alle):

* Rasberry Pi 2 Model B
* XBOOM Lautsprecher
* Gold+ Memory SD Card
* Sienoc Mini Mikrofon
 
Festlegen eines Projektziels (Alle):
Unterstützung des Lernens und der Lehre am HHZ, sowohl für Studierende als auch für Professoren
Es soll ein allumfassender HHZ Service mithilfe eines sprachgesteuerten Raspberry Pi umgesetzt werden.

Verantwortlichkeiten definieren:

* Implementierung: Sebastian & Johan
* Doku: Max & Sascha
* Service Design: Martin & Tobias

Wir wollen alle anfallenden Aufgaben in einer Art "Pair Programming" erledigen. Hierzu wurden Verantwortliche für die drei Hauptbereiche definiert. Je nach Arbeitsaufkommen werden die Aufgaben auf andere, nicht verantwortliche Teammitglieder übertragen. Vor dem Hackathon wird mehr Theoriearbeit verlangt und beim Hackathon wird der Fokus auf der Implementierung liegen.

Vorgehensweise:

* Forschungsgruppe als Living Lab 
* Alle Teilnehmer sind Studierende am HHZ

[Brainstorming Serviceidee (alle)](https://bitbucket.org/iotalexahhz/iotalexahhz/downloads/HHZ%20Service%20Living%20Lab_Brainstorming.jpeg)

# 06.06.2015 - Let's getting started

Seit dem letzten Termin arbeiteten sich die einzelnen Mitglieder in die Themen rund um Sprachsteuerung und das Projektumfeld ein. Hierfür wurde der Rasberry Pi von Sebastian soweit vorbereitet, dass der Alexa Sprachassistent darauf läuft. Näheres hierzu können Sie der Wikiseite [Alexa lebt!](https://bitbucket.org/iotalexahhz/iotalexahhz/wiki/Alexa%20lebt!) entnehmen.

Parallel hierzu wurde von Johan eine Bluemix Organisation aufgesetzt. Hierzu finden Sie weitere Infos auf der Wiki-Seite [TODO](Link URL). Mit dieser haben sich alle Mitglieder auseinander gesetzt und erste praktischen Erfahrungen mit dem für alle neuen Tool gesammelt. Was Bluemix genau ist und wie wir dieses Tool einsetzen erfahren Sie [TODO hier](Link URL).

Um die gewonnen Erkenntnisse während des Hackathons wurde von Sascha und Max dieses Wiki in Bitbucket aufgesetzt. Hier möchten wir Ihnen unser erworbenes Wissen während der zwei Tage des Hackathons und auch die theoretischen Grundlagen aufzeigen.

Um unsere Hackathonimplementierungen in eine Richtung zu leiten, die den Studenten am HHZ echten Mehrwert liefert, wird von Tobias und Martin ein Serviceentwurf aufgezeichnet. Dieses finden Sie auf der [TODO entsprechenden Wiki-Seite](Link URL). 