**[Zurück zu Home](Home)**

Die Phase **Experimentation** gehört zu dem [Living Lab Prozess](Living Lab HHZ).

**Aufbau**  

[TOC]





##IBM-Bluemix registrieren und aufsetzen##
In diesem Kapitel wird dargestellt, wie die Registrierung und das Aufsetzen bei IBM Bluemix geklappt hat (oder auch nicht) und die Dokumentation hierzu:
 
###[Probleme bei der Registrierung](IBM Bluemix - erste Schritte)
 
###[Registrierung erfolgreich](IBM Bluemix - Registrierung)

##Raspberry Pi - Installation & Konfiguration
In diesem Kapitel ist dokumentiert, welche Schritte bei der Installation und Konfiguration durchgeführt wurden sind.

[Link zu Raspberry Pi - Installation & Konfiguration](Raspberry Pi)

##Alexa lebt!##
In diesem Kapitel wurde die Installation von Alexa dokumentiert, wie *Sie zum Leben erweckt wird*

[Link zu Alexa lebt!](Alexa lebt!)

##MVP-Implementierung##
Unter diesem Punkt ist die Implementierung des MVP dargestellt.

[Link zu MVP-Implementierung](MVP implementieren)



