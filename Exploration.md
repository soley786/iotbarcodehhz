**[Zurück zu Home](Home)**

Die Phase **Exploration** gehört zu dem [Living Lab Prozess](Living Lab HHZ).

**Aufbau**  

[TOC]






##Brainstorming##
In diesem Kapitel wird dargestellt, wie die Gruppe den Hackathon angegangen ist. Also wie können Services für Alexa aussehen (bspw. Ablauf), welche Services wollen wir anbieten, wie soll die Struktur der Dokumentation aussehen. Die gefundenen Inhalte fließen in die weiteren Phasen [Exploration](Exploration) und [Experimentation](Experimentation) ein und können im Detail in der Dokumentation dieser Phasen nachgelesen werden.

**[Link zum Brainstorming](Brainstorming)**

##Service-Entwurf##
In diesem Unterpunkt wurde dokumentiert, welche Aspekte bei der Entwicklung von Services, die über die natürliche Sprache angesprochen und gesteuert werden, berücksichtig werden müssen.
Als Verbindung zu den Inhalten der Vorlesung Service Science and Engineering wurde per Lean Canvas (Abwandlung vom Business Model Canvas) dargestellt, welche Probleme, Zielgruppen, Solutions und andere Aspekte im Zusammenhang mit den HHZ-Services vorkommen können. Zudem werden die Nutzen für die User herausgearbeitet.


**[Link zum Service-Entwurf](Service Design)**

##MVP - bestimmen##
In diesem Kapitel wird beschrieben, wie die Gruppe von der Gesamtheit aller gefundenen HHZ-Services, die über Alexa angesprochen werden können, der MVP bestimmt wurde und was der Service macht.

**[Link zum MVP - bestimmen](MVP)**

 	
