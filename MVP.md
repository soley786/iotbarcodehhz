**[Zurück zu Home](Home)
**
# MVP - Minimum Viable Product #
Die Ergebnisse des Brainstormings sind in folgender Mindmap dokumentiert. Das MVP ist rot markiert.

![HHZ Service Living Lab_Brainstorming_V4_markiert.jpg](https://bitbucket.org/repo/gLkBzg/images/3773194565-HHZ%20Service%20Living%20Lab_Brainstorming_V4_markiert.jpg)

# Auswahl des MVPs #
Im Brainstorming wurden Ideen für HHZ Services gesammelt. Neben allgemeinen Services wurde nach Smart Room Services, Mittagessen Services und Services zur Abfrage der Raumbelegung gruppiert.
Als MVP wurde der Service "Raumbelegung mit Webuntis abfragen" gewählt. Ein MVP ist darauf ausgelegt, ein minimales, funktionsfähiges Produkt, dass zur Machbarkeitskeitsanalyse geeignet ist, zu erstellen. 

Die Raumbelegung mit Webuntis beinhaltet alle wesentlichen Prozessschritte der Lösung. Alexa wird vom Nutzer angesprochen, erhält einen Befehl zu Ausführung. Dieser wird an den Amazon Voice Service (AVS) übermittelt, verarbeitet und an unseren Service im Bluemix übergeben. Der Bluemix Service übernimmt die weiteren Ausführungsschritte und gibt die Antwort an AVS zurück. AVS codiert diese in für den Nutzer verständliche Sprache und übermittelt die Antwort an den Alexa Client. Dieser gibt diese aus. Einen Überblick bietet auch das Sequenzdiagramm im Bereich [Service-Entwurf](Service Design).

Durch diese Schritte sind alle umsetzungskritischen Teile der Lösung vorhanden und lassen sich für andere Services adaptieren. Deshalb ist der Service "Raumbelegung mit Webuntis abfragen" als MVP geeignet.

Die erfolgreiche Umsetzung dieses Services zeigt, dass die Sprachsteuerung genutzt werden kann, um einen Mehrwert insbesondere für den Nutzer zu generieren. Dieser besteht in der intiutiven, (natürlich sprachlichen) Bedienung, der schnellen Rückmeldung und der Integration ins HHZ Umfeld.

Der Nutzer hat im Einzelnen folgenden Mehrwert durch den MVP:

* spart sich den Aufwand über PC oder Handy WebUntis aufzurufen
* muss nicht mit der (umständlichen) GUI von WebUntis agieren
* Zeitersparnis, da der Nutzer die einzelnen Räume auf dem Campus nicht aufsuchen muss
* Verhindert eine evtl. Störung (durch das Öffnen der Türe), wenn nicht klar ist, ob der Raum belegt ist.

Die einzelnen Mehrwerte sind auch in den Modellen [Lean Canvas](Service Design) und dem nachfolgenden Value Proposition Canvas enthalten.

Die Implementierung des Raumbelegungsservice zeigt, dass die Sprachsteuerung auch für weitere Services genutzt werden kann. Dazu ist lediglich die Entwicklung einer weiteren Bluemix-Komponente erforderlich.

# Value Proposition Canvas des MVP #

Im Rahmen des [Service-Entwurf](Service Design) wurde ein Lean Canvas zu den Kernproblemen auf denen die HHZ-Services basieren erstellt. Der Zuschnitt des MVPs erfolgte auf Basis der Brainstorming Ideen.
Zur genaueren Spezifizierung des MVPs wurde ein Value Proposition Canvas zum MVP entwickelt.
Dieser zeigt konkrete Probleme, Jobs und Gewinne der Nutzergruppe Studierende und nimmt Bezug wie das MVP diese Probleme löst und die Gewinne erzeugt.

![ValuePropositionCanvas_MVP_1.jpg](https://bitbucket.org/repo/gLkBzg/images/2111667707-ValuePropositionCanvas_MVP_1.jpg)

![ValuePropositionCanvas_MVP_2.jpg](https://bitbucket.org/repo/gLkBzg/images/1400868934-ValuePropositionCanvas_MVP_2.jpg)