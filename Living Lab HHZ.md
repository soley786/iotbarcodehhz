**[Zurück zu Home](Home)**

Wir als Projektteam haben uns dazu entschlossen, den Hackathon analog der in der Vorlesung IoT dargestellte Forschungsmethode "Living Lab" aufzubauen, daher haben wir das ganze **Living Lab HHZ ** getauft.

Vom Vorgehen halten wir uns dabei an der in der Vorlesung dargestelltem Living Lab Prozess, der aus 4 Phasen besteht:


![Living Lab Prozess.png](https://bitbucket.org/repo/gLkBzg/images/1146103036-Living%20Lab%20Prozess.png)
[Quelle: Living Lab Prozess](http://www.ausmt.org/imagefiles/134/134LEFigure1.png)

Die erste Phase [**Co-Creation**](Co-Creation) bildet die Grundlage für den Hackathon und wurde durch die Vorlesung von Prof. Decker abgedeckt. Hier wurden die theoretischen Grundlagen von IoT (Allgemeine, Kommunikation, Hardware, etc.) geschult und auch mögliche Buisness Models sowie Anwendungsbereiche erläutert.

Durch diese Phase wurden wir auf bestehende Probleme, Ideen und Bedürfnisse im Zusammenhang zu IoT hingewiesen.

Aufbauend auf der ersten Phase, befasst sich die zweite Phase **[Exploration](Exploration)** mit den eigentlichen Services. In dieser Phase wurde u.a. das Brainstorming unternommen, welche Services wir mit Alexa anbieten wollen und was unser MVP ist. Zudem finden sich in dieser Phase die Verbindung zu den Inhalten der IoT-Vorlesung, als auch zu anderen Vorlesungen des HHZ, wie bspw. Service Science and Engineering.

In der dritten Phase **[Experimentation](Experimentation)** befindet sich u.a. das Aufsetzen von Alexa auf dem Raspberry Pi, sowie die Regisitrierung und Einarbeitung in Bluemix. Zudem werden hier die einzelnen Implementierungsschritte sowie die verschiedenen Tests hinterlegt.

In der vierten und letzten Phase **[Evaluation](Evaluation)** wird der implementierte MVP validiert und ein Ausblick gegeben, welche Funktionalitäten noch implementiert werden können. Diese Phase wird in dem Projekt nur angeschnitten, da die Zeit für eine ausführliche Evaluation nicht ausreicht.

Über die einzelnen Links auf dieser Wiki-Seite gelangen Sie zu den dokumentierten Phasen.



