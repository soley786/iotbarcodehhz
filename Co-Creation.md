**[Zurück zu Home](Home)**

Wie bereits im [Living Lab HHZ](Living Lab HHZ) beschrieben, wurde die Co-Creation Phase durch die [Vorlesung](https://relax.reutlingen-university.de/course/view.php?id=7923) von Herrn Decker abgedeckt.

Nachfolgend werden noch die Besonderheiten eines Sprachassisstenten im IoT-Umfeld dargestellt. In dem Projekt wird Sprachassisstent mit Alexa gleichgesetzt, da die Implementierung von Alexa vorgegeben war:

In diesem Projekt wird ein Sprachassistent für das Herman Hollerith Zentrum in Böblingen implementiert. Das Besondere an der Umsetzung ist der Verzicht auf ein graphisches Interface für den Benutzer (GUI). Der Benutzer kommuniziert mit dem Computersystem über die **natürliche Sprache** was als Voice User Interface (VUI) bezeichnet wird. Dadurch rückt die **Technologie weiter in den Hintergrund (unsichtbare Technologie)** und der Benutzer kann **(fast) ohne Medienbruch** mit dem System kommunizieren. Kommuniziert kann mit der direkten Umgebung, indem sich der Nutzer befindet.

Dies bedeutet für den Benutzer, dass er die Befehle für das System kennen muss, bzw. das System dem User ein sprachliches Feedback geben muss und nicht über ein GUI geführt wird und hierüber die notwendigen Schritte auswählen kann. Auf das muss beim Design und der Umsetzung eines solchen Services geachtet werden. Weiteres zu den Herausforderungen findet sich [in unserem Wiki](https://bitbucket.org/iotalexahhz/iotalexahhz/wiki/Service%20Design)  


###Einordnung in geschäftsrelevanten Bereich nach E.Fleisch:###

In der Vorlesung wurden Inhalte aus den Arbeiten von Edgar Fleisch (u.a. Elgar Fleisch, What is the Internet of Things? An Economic Perspective, Auto-ID Labs White Paper, January 2014) behandelt, der in diesen u.a. dargestellt hat, in welche geschäftsrelevanten Bereiche sich IoT-Anwendungen oder System einordnen lassen:

* Machine-to-Machine (M2M)
* Integration mit Nutzern

Wie bereits oben erwähnt werden wir die Implementierung eines Sprachassisstenten vornehmen, was klar in den Bereich **Integration mit Nutzern** fällt. Der M2M-Bereich ist in diesem Fall nicht relevant, da die Interaktion mit der (natürlichen) Sprache für die Integration mit Nutzern vorgesehen ist und nicht bspw. zur Kommunikation zwischen zwei Maschinen.

Beide Themenkomplexe, Business Models und das Internet der Dinge werden sehr anschaulich im [Video von der Universität St.Gallen](https://www.youtube.com/watch?v=kYQ_PHOCjyg) behandelt.
