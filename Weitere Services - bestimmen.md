**[Zurück zu Home](Home)**

![Brainstorming_Verspätungen.jpg](https://bitbucket.org/repo/gLkBzg/images/1763077581-Brainstorming_Versp%C3%A4tungen.jpg)

Als weiteren zu realisierenden Service entschlossen wir uns für "Abfrage Verspätungen von Personen".
Dieser ermöglicht die Abfrage, ob alle Teilnehmer einer Vorlesungsveranstaltung pünktlich zu Beginn anwesend sein werden. 

Folgendes Szenario ist denkbar:

Der Professor sitzt in seinem Büro kurz vor Beginn seiner Vorlesung. Über den Service prüft er, ob es bekannte Verspätungen unter seinen Teilnehmern gibt. Er stellt fest, dass die Hälfte seiner Studenten nicht pünktlich präsent sein wird, bspw. durch einen Bahnausfall. Daraufhin entschließt er sich, die Vorlesung erst 20 Minuten später zu beginnen und die neu hinzugewonnene Zeit für andere Tätigkeiten zu nutzen.

Studenten haben die Möglichkeit über den Instant Messenger Telegram (vergleichbar mit WhatsApp) eine Nachricht an einen definierten Chatpartner (Telegram Bot) mit dem Fach, welches nicht pünktlich zu Beginn besucht werden kann, zu senden.
Bei Abfrage des Professors werden alle eingetroffenen Nachrichten zu einem bestimmten Fach ausgewertet und die Anzahl ausgegeben. 

Als mögliche Erweiterung ist denkbar, dass Studenten zusätzlich zum Vorlesungsnamen eine Begründung für die Verspätung mitsenden, welche dann zusammen mit dem Teilnehmernamen bei Bedarf ausgegeben wird.