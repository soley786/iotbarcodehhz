** Zurück zu [Home](Home)**

**Hier befindet sich die Dokumentation, wie Raspberry Pi installiert und konfiguriert wurde**

**Aufbau** 

[TOC]


#####Benötigte Hardware:

* Raspberry Pi 2 (Model b+)  
* Micro USB Power Kabel  
* Micro SD Karte mit minimum 8GB Speicherplatz  
* Ethernet Kabel / WIFI Dongle  
* USB 2.0 Microphone  
* External Speaker  
* Tastatur & Maus mit WIFI / USB Anschluss  
* HDMI Kabel  
* Monitor  

#####Benötigte Software:
 
* Betriebssystem: „RASPBIAN JESSIE“ (Image Datei ) herunterladen  
     * https://www.raspberrypi.org/downloads/raspbian/
* SD Karte mit einem Image Programm beschreiben: „Win32 Diskmanager“ herunterladen
     * http://www.chip.de/downloads/Win32-Disk-Imager_46121030.html
* (Optional) – SD Karte formatieren: „SD Formatter“ herunterladen
     * http://www.chip.de/downloads/SD-Formatter_72605634.html
* Telnet Client für Windows Nutzer – PuTTY installieren und einreichten
     * http://www.chip.de/downloads/PuTTY_12997392.html


##### Raspberry Pi starten und konfigurieren

* Raspberry Pi 2 anschließen und starten
* Raspberry Pi 2 Speicherplatz erweitern
     * Unter dem „Raspberry Pi Menü“ ->  „Einstellungen“ -> „Raspberry Pi Configuration“ klicken
     * Unter dem Tab „System“ -> „Filesystem“ -> Expand Filesystem klicken
     * Im Tab „Interfaces“ -> SSH = Enable, Serial = Enable, der Rest ist Disable
     * Gegebenfalls Hostname und Password ändern
* Raspberry Pi 2 neustarten

##### SSH
* Terminal öffnen (MAC OSX)
* PuTTY verwenden (WINDOWS)
* Befehl „ifconfig“ eingeben
* IP Adresse rauskopieren
* Befehl ssh pi@<Raspberry Pi IP Adresse > eingeben
     * Beispiel: ssh pi@192.168.178.36
* Passwort vom Raspberry Pi eingeben

![Johan.JPG](https://bitbucket.org/repo/gLkBzg/images/2292993460-Johan.JPG)

##### VLC installieren
Befehl eingeben:

```
#!python
sudo apt-get install vlc-nox vlc-data 
```
![johan2.png](https://bitbucket.org/repo/gLkBzg/images/3052485983-johan2.png)

Falls VLC bereits installiert wurde, muss man zwei Bibliotheken entfernen
Befehl eingeben:
```
#!python
sudo apt-get remove --purge vlc-plugin-notify 
sudo rm /usr/lib/vlc/plugins/codec/libsdl_image_plugin.so
 
```
Umgebungsvariablen für VLC setzen
Befehle eingeben:
```
#!python
export LD_LIBRARY_PATH=/usr/lib/vlc 
export VLC_PLUGIN_PATH=/usr/lib/vlc/plugins 
 
```
Überprüfen ob die Umgebungsvariablen erfolgreich gesetzt wurden

```
#!python
echo $LD_LIBRARY_PATH 
echo $VLC_PLUGIN_PATH 

```

#####NodeJS installieren
Befehl eingeben:
```
#!python
sudo apt-get install nodejs 

```
![johna3.png](https://bitbucket.org/repo/gLkBzg/images/1656895308-johna3.png)

#####Java Development Kit installieren

[JDK8 herunterladen](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)

jdk-8u91-linux-arm32-vfp-hflt.tar.gz in den Ordner „opt“ extrahieren
Befehl eingeben:
```
#!python
sudo tar zxvf jdk-8u91-linux-arm32-vfp-hflt.tar.gz -C /opt

```
Java und javac als default setzen
```
#!python
sudo update-alternatives --install /usr/bin/javac javac /opt/jdk1.8.0_91/bin/javac 1

sudo update-alternatives --install /usr/bin/java java /opt/jdk1.8.0_91/bin/java 1 

```
![john4.png](https://bitbucket.org/repo/gLkBzg/images/3185261665-john4.png)
![John89.JPG](https://bitbucket.org/repo/gLkBzg/images/2540015239-John89.JPG)


#####Maven installieren

[Maven herunterladen:](https://maven.apache.org/download.cgi)

Maven extrahieren & Standort festlegen:
```
#!python
sudo tar zxvf apache-maven-3.3.9-bin.tar.gz -C /opt 
export M2_HOME=/opt/apache-maven-3.3.9
export PATH=$PATH:$M2_HOME/bin 

```
Maven Version Überprüfen

![johna64.png](https://bitbucket.org/repo/gLkBzg/images/2036519914-johna64.png)



