**[Zurück zu Home](Home)**

Die Phase **Evaluation** gehört zu dem [Living Lab Prozess](Living Lab HHZ).

**Aufbau**  

[TOC]


###Validierung des MVP

Der implementierte MVP ist funktionsfähig und bietet die Nutzer-Mehrwerte, wie sie in der Exploration-Phase beim [MVP bestimmen](MVP) herausgearbeitet wurden.
Die Implementierung ist soweit fortgeschritten, dass nicht nur ein einfacher Mock-Up aufgerufen wird, sondern tatsächlich der Kalendar von WebUntis eingebunden ist.
Die Implementierung kann in der Experimentation-Phase bei [MVP implementieren](MVP implementieren) nachgelesen werden.

Wie schon im [HHZ Living Lab](Living Lab HHZ) beschrieben, reicht die Zeit für eine detaillierte Evaluation, mit bspw. Focus Groups oder Expertenbefragungen nicht aus.


###Ausblick

Aus Sicht der Gruppe wäre eine noch tiefere Integration eines Sprachassisstenten in die "Umgebung" sinnvoll und auch zukunftsweisend.
Das bedeutet, dass ein Sprachassisstent ein ständiger Begleiter ist, der aktiv nicht wahrgenommen wird, sondern nur immer dann in Erscheinung tritt, wenn er 

* aktiv angesprochen wird (Sprachassisstent nimmt Information entgegen)
* selber aktiv wird (Sprachassisstent gibt Informationen aus)

Der passive Part, der Sprachassisstent nimmt Informationen entgegen, wurde im Hackathon untersucht.
Dies passiert bspw. mittels Codewort für die Aktivierung oder ganz einfach über ein Knopfdruck auf einer GUI.

Der aktive Part eines Sprachassisstent wurde nicht untersucht. Hier wird ein Sprachassisstent selber aktiv, entweder aufgrund von empfangenen Informationen, oder aufgrund gelernter Verhaltensmuster. Hierfür muss der Sprachassisstent immer angeschaltet sein und seine Umgebung "überwachen". 

Ein beispielhaftes Szenario wäre:
Alexa oder ein anderer Sprachassisstent nimmt eine Konversation auf, in der es um ein Kinobesuch geht. Aufgrund des gelernten Verhaltsmuster des Users, sowie der Einbeziehung weiterer Services, wie bspw. Prüfung des Terminkalenders des Users, reserviert der Sprachassisstent selbständig das Kino und gibt dem User dies als Information aus.

Das dargestellte Szenario wäre eine sehr hohe Form des Eingriffs der Technik in die *Welt des Menschen*. Gleichzeitig wäre die Technologie tatsächlich unsichtbar im Hintergrund aktiv, bis zu dem Zeitpunkt an dem der User die Bestätigung der Reservierung erhält.

Bei dieser Art der Verwendung von IoT Sprachassisstenten muss es klare Regeln geben. Zudem muss auch eine hohe Feedback-Integration beinhalten:

* Ab welchen Zeitpunkt ist es klar, dass ein Kinobesuch forciert wird?
* Darf der Sprachassisstent selbstständig eine Kinoreservierung durchführen?
* Muss vorher der User um Erlaubnis gefragt werden?
* War die Reservierung richtig? (Feedback)

