**[Zurück zu Home](Home)**

Nachdem das Unboxing und die grobe Einteilung durchgeführt wurde, gingen wir nun her und meldeten uns bei IBM Bluemix an.

Die Anleitung, hinterlegt im relax, sieht ja schonmal einfach aus: 

1. Registrieren
2. Log-In
3. Cut & Paste Promo Code in die rechte obere Ecke

Tatsächlich aber klappte nur der Log-In auf Anhieb.
Nach der Registrierung kam die Bestätigungsmail einfach nicht. 
Es dauerte eine knappe Stunde bis diese verschickt wurde und die Registrierung dann abgeschlossen werden konnte:
![bluemix5.JPG](https://bitbucket.org/repo/gLkBzg/images/536258762-bluemix5.JPG)

Die größten Probleme gab es bei der Hinterlegung des Promo-Codes.
In der Anleitung dazu steht:

*"click the number of days remaining on your account in the upper right corner, and click promo code."*

Leider gibt es diese Anzeige nicht. 

In der rechten oberen Ecke können die Profil-Informationen aufgerufen, aber kein Promo-Code hinterlegt werden.
Des Weiteren können hier auch nur die Regionen *Vereintes Königreich, Sydney und Vereinigte Staaten* ausgewählt werden.
![Bluemix.png](https://bitbucket.org/repo/gLkBzg/images/560922382-Bluemix.png)

Selbst bei den Unterpunkten "Konto" und "Status" deutet nichts auf die Eingabe des Promo-Codes hin.
![Bluemix2.png](https://bitbucket.org/repo/gLkBzg/images/3863161382-Bluemix2.png)


Weiterhin funktioniert der Wechsel zwischen neuer & alter Ansicht nur manchmal:
![Bluemix3.png](https://bitbucket.org/repo/gLkBzg/images/3870437224-Bluemix3.png)


Und einige Links sind nicht aktiviert, wie der Link zu "IBM ID bearbeiten".
![Bluemix4.png](https://bitbucket.org/repo/gLkBzg/images/716304768-Bluemix4.png)


Nach stundenlangem Klicken und suchen auf den Bluemix-Seiten und innerhalb der einzelnen Kategorien u.a. "Dashboard", "Konto", "Status" einhergehend mit einem Wechsel des Browser, hat der Autor es aufgegeben, den Promo-Code zu hinterlegen

![angry-desk-flip.png](https://bitbucket.org/repo/gLkBzg/images/3389641610-angry-desk-flip.png)

# EDIT: #
Nach Rücksprachen mit einigen Kollegen, sollte die Oberfläche wie folgt aussehen:
![IMG_6774.JPG](https://bitbucket.org/repo/gLkBzg/images/3931407899-IMG_6774.JPG)

Wie in meinem zweiten Screenshot zu sehen ist, fehlen bei mir aber sämtliche Unterkategorien zu "Konto".

Aus diesem Grund hat sich der Autor dazu entschieden, eine nochmalige Registrierung mit einer anderen Mail-adresse durchzuführen.

##Und siehe da, rechts oben kann man nun tatsächlich den Promo-Code hinterlegen##
![bluemix8.png](https://bitbucket.org/repo/gLkBzg/images/3116479361-bluemix8.png)

![strichmann.png](https://bitbucket.org/repo/gLkBzg/images/4216859375-strichmann.png)
