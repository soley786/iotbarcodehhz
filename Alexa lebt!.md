**[Zurück zu Home](Home)**

Auf dieser Wiki-Seite wird dargestellt, wie Alexa auf dem Raspberry Pi "zum Leben erweckt wurde":

**Agenda**

[TOC]

## Vorgehen

Der Amazon Alexa Javaclient wurde auf dem Raspberry Pi nach folgender Anleitung installiert:

[Amazon AVS Raspberry Pi](https://github.com/amzn/alexa-avs-raspberry-pi)

Im Detail wurden folgende Schritte durchgeführt:
####**Registrierung für Developer Amazon Account**###

URL: https://developer.amazon.com/home.html

![alexa.png](https://bitbucket.org/repo/gLkBzg/images/2835498917-alexa.png)

####**Alexa Voice Service Quellcode herunterladen & auf Raspberry Pi kopieren**###

URL: https://github.com/amzn/alexa-avs-raspberry-pi/archive/master.zip

####**Produkt registrieren und Security Profil erstellen**####

Damit Alexa (samt Core-Services) genutzt werden kann, ist es notwendig, dass Produkt zu registrieren.
Hierfür muss auf der Amazon-Developer Seite die Produktregistrierung durchgeführt und ein Security Profil erstellt werden. Dazu wird das Device angelegt und konfiguriert.

![1.JPG](https://bitbucket.org/repo/gLkBzg/images/2149123859-1.JPG)

Nachdem die Registrierung bei Amazon Developer erfolgreich war

* Klicken Sie auf "ALEXA" > Alexa Voice Service > "Get Started >" - Button

![2.JPG](https://bitbucket.org/repo/gLkBzg/images/2748822065-2.JPG)

Klicken Sie auf "Register a Product Type" und anschließend auf "Device".

![124.JPG](https://bitbucket.org/repo/gLkBzg/images/3474421023-124.JPG)

Tragen Sie beim "Device Type Info" in den folgenden Felder

* Company Name: HHZ als Beispiel
* Device Type: my_device
* Display Name: My Device
* "Next" klicken

![alexa11.png](https://bitbucket.org/repo/gLkBzg/images/3590624398-alexa11.png)

* Klicken Sie bei Security Profile auf die Dropdownliste "Select Security Profile" und wählen "Create a new profile"
* Klicken Sie "Next"

![5.JPG](https://bitbucket.org/repo/gLkBzg/images/1999849150-5.JPG)

Tragen Sie im General Tab in folgenden Felder:

* Security Profile Name: Alexa Voice Service Sample App Security Profile*
* Security Profile Description: Alexa Voice Service Sample App Security Profile Description*
* Klicken Sie "Next"

![alexa13.png](https://bitbucket.org/repo/gLkBzg/images/190484311-alexa13.png)

Anschließend werden Client ID und Client Secret generiert

* Klicken Sie auf den Web Settings Tab
* Wählen Sie in der Dropdownliste "Alexa Voice Service Sample App Security Profile" aus und klicken Sie auf "Edit"

![7.JPG](https://bitbucket.org/repo/gLkBzg/images/1910850806-7.JPG)

* Tragen Sie in den folgenden Textfelder im Bereich Web Settings Tab:
* Allowed Origins: (auf Add Another klicken) https://localhost:3000 
* Allowed Return URLs: (auf Add Another klicken) https://localhost:3000/authresponse

![allow.JPG](https://bitbucket.org/repo/gLkBzg/images/587293056-allow.JPG)

* Klicken Sie auf "Next"

![AVS_IMAGE.JPG](https://bitbucket.org/repo/gLkBzg/images/3105570984-AVS_IMAGE.JPG)

[Image File](https://camo.githubusercontent.com/7198eaa71b41263a046a73253e383eae36aba0d3/68747470733a2f2f646576656c6f7065722e616d617a6f6e2e636f6d2f7075626c69632f62696e61726965732f636f6e74656e742f67616c6c6572792f646576656c6f706572706f7274616c7075626c69632f736f6c7574696f6e732f616c6578612f616c6578612d766f6963652d736572766963652f696d616765732f7265666572656e63652d696d706c656d656e746174696f6e2d696d6167652e706e67) herunterladen

![![CaptureImage_Avatar.JPG](https://bitbucket.org/repo/gLkBzg/images/1989077172-CaptureImage_Avatar.JPG)
](https://bitbucket.org/repo/gLkBzg/images/2182055120-CaptureImage_Avatar.JPG)

Im Bereich Device Details sind folgende Schritte zu erledigen:

* Image: heruntergeladene Bilddatei hochladen
* Category: "Other" in der Dropdownliste auswählen
* Descripten: "Alexa Voice Service sample app test"
* What is your expected timeline for commercialization?: "Longer than 4 months / TBD"
* How many devices are you planning to commercialize?: "0"
* Klicken Sie auf "Next"

![alexa20.png](https://bitbucket.org/repo/gLkBzg/images/427145354-alexa20.png)

Wählen Sie bei "Enable Amazon Music?" die Option "No" aus und klicken Sie auf "Submit".

![11.JPG](https://bitbucket.org/repo/gLkBzg/images/3653631030-11.JPG)

Ihr Gerät wurde nun erfolgreich unter Registered Products registriert.



####**Generate self-signed certificates**####

SSL installieren

Befehl eingeben:

```
#!python
sudo apt-get install openssl 
```
![alexa9.png](https://bitbucket.org/repo/gLkBzg/images/1056074850-alexa9.png)

Installation verifizieren

```
#!python
whereis openssl
```

![alexa10.png](https://bitbucket.org/repo/gLkBzg/images/3505351709-alexa10.png)

```
#!python
cd <REFERENCE_IMPLEMENTATION>/samples/javaclient - //your sample apps location
```

Die Skriptdatei "ssl.cnf" bearbeiten (Datei könnte so aussehen)

![Capture.JPG](https://bitbucket.org/repo/gLkBzg/images/386340883-Capture.JPG)

Folgender Befehl macht die geänderte Skriptdatei ausführbar

```
#!python
chmod +x generate.sh
```

Zertifikat Skript ausführen

```
#!python
./generate.sh
```

Anschließend muss in der Konsole folgende Informationen eingetragen werden:

* Product ID: my_device
* Serial number: 123456
* Password: talktome (als Beispiel)

```
#!python
<REFERENCE_IMPLEMENTATION>/samples/companionService/config.js. 
```



Folgende Felder müssen in der Config.js Datei angepassten werden:

```
#!python
Set sslKey to <REFERENCE_IMPLEMENTATION>/samples/javaclient/certs/server/node.key
Set sslCert to <REFERENCE_IMPLEMENTATION>/samples/javaclient/certs/server/node.crt
Set sslCaCert to <REFERENCE_IMPLEMENTATION>/samples/javaclient/certs/ca/ca.crt
```

Folgende Änderungen müssen in der config.json Datei durchgeführt werden:

```
#!python
<REFERENCE_IMPLEMENTATION>/samples/javaclient/config.json. 
```

```
#!python
Set companionApp.sslKeyStore to <REFERENCE_IMPLEMENTATION>/samples/javaclient/certs/server/jetty.pkcs12
Set companionApp.sslKeyStorePassphrase --> Beispiel Passwort setzen: "talktome"
Set companionService.sslClientKeyStore to <REFERENCE_IMPLEMENTATION>/samples/javaclient/certs/client/client.pkcs12
Set companionService.sslClientKeyStorePassphrase --> Beispiel Passwort setzen: "talktome"
Set companionService.sslCaCert to <REFERENCE_IMPLEMENTATION>/samples/javaclient/certs/ca/ca.crt
```


####**Install Dependencies**####

In den Pfad companionService wechseln

```
#!python
cd <REFERENCE_IMPLEMENTATION>/samples/companionService
```

npm installieren 

```
#!python
npm instal
```


####**Enable Security Profile**####
URL https://developer.amazon.com/lwa/sp/overview.html aufrufen

![alexa25.png](https://bitbucket.org/repo/gLkBzg/images/2178679945-alexa25.png)

In der Dropdownliste "Alexa Voice Service Sample App Security Profile" auswählen und "Confirm" klicken

![13.JPG](https://bitbucket.org/repo/gLkBzg/images/1030981583-13.JPG)

Im Textfeld "Consent Privacy Notice URL": http://example.com eingeben

![14.JPG](https://bitbucket.org/repo/gLkBzg/images/1099069239-14.JPG)

Auf "Show Client ID and Client Secret" klicken

![alexa28.png](https://bitbucket.org/repo/gLkBzg/images/1475672643-alexa28.png)

"Show Client ID and Client Secret" werden angezeigt

####**Updating Config-Files**####

Die Datei config.js im Ordner companionService öffnen und bearbeiten:

```
#!python
<REFERENCE_IMPLEMENTATION>/samples/companionService/config.js   
```

* clientId: <client ID>
* clientSecret: <client secret> 
* products: <product number>

![clientid_secred_product.JPG](https://bitbucket.org/repo/gLkBzg/images/1106058600-clientid_secred_product.JPG)

Datei abspeichern

Die Datei config.json im Ordner javaclient öffnen und bearbeiten:

```
#!python
<REFERENCE_IMPLEMENTATION>/samples/javaclient/config.json   
```

* productId: "my device" eingeben.
* dsn: "123456" eingeben
* provisioningMethod: "companionService" eingeben


Die Datei pom.xml im Ordner javaclient öffnen und bearbeiten:

```
#!python
<REFERENCE_IMPLEMENTATION>/samples/javaclient/pom.xml   
```

```
#!python
<dependency>
  <groupId>net.java.dev.jna</groupId>
  <artifactId>jna</artifactId>
  <version>4.1.0</version>
  <scope>compile</scope>
</dependency>
```

![xml.JPG](https://bitbucket.org/repo/gLkBzg/images/2057040299-xml.JPG)

Datei abspeichern

####**Alexa starten**####

In den Ordner "companionService wechseln und Server starten.
In der Konsole folgende Befehle ausführen:

```
#!python
cd <REFERENCE_IMPLEMENTATION>/samples/companionService
npm start
```

**Client starten**

Neuer Tab Fenster öffnen und in den Ordner javaclient wechseln

```
#!python
cd <REFERENCE_IMPLEMENTATION>/samples/javaclient 
npm start
```

**Build the App** 

Bevor man einen Build erzeugt, sollte das Projekt validiert werden und die benötigen Informationen vorhanden sind.

```
#!python
mvn validate
```
Die Dependencies werden heruntergeladen und ein Build wird erzeugt. 

```
#!python
mvn install 
```

**Client App starten**

Nach dem die Installation erfolgreich war, sehen Sie einen "Build Success" im Terminal Fenster. Anschließend kann die Client App ausgeführt werden.

```
#!python
mvn exec:exec
```

![mvn_exec_exec.JPG](https://bitbucket.org/repo/gLkBzg/images/2840167671-mvn_exec_exec.JPG)

![gui_Alexa.JPG](https://bitbucket.org/repo/gLkBzg/images/3572966032-gui_Alexa.JPG)

Alexa Gui wird gestartet

![popup_localhost.JPG](https://bitbucket.org/repo/gLkBzg/images/3308643887-popup_localhost.JPG)

Anschließend muss das Gerät registriert werden. Hierbei soll die URL kopiert und in einem Browser geöffnet werden

![amazon_account.JPG](https://bitbucket.org/repo/gLkBzg/images/64126433-amazon_account.JPG)

Der nächste Schritt ist die Authorisierung bei Amazon

![Ok.JPG](https://bitbucket.org/repo/gLkBzg/images/2920919845-Ok.JPG)

Klicken Sie auf "Okay"

![device_token_ready.JPG](https://bitbucket.org/repo/gLkBzg/images/703729870-device_token_ready.JPG)

Nun wurde das Gerät erfolgreich registriert und kann genutzt werden.

![gui_Alexa.JPG](https://bitbucket.org/repo/gLkBzg/images/3688085034-gui_Alexa.JPG)

* Start Listening button : Gespräch wird gestartet
* Stop Listening button :  Gespräch wird beendet


##Anmerkungen##
Ausgehend der aktuellen Raspbian Version "Jessie" sind bereits die Komponenten Java und Node.js vorinstalliert.
Man könnte meinen, dass deswegen alles reibungslos funktioniert. Mitnichten.

Außgehend der Beschreibung müssen beide Komponenten in der jeweils aktuellen Version nachinstalliert werden.
Dazu wird das JDK von Oracle heruntergeladen und als zu verwendendes markiert. 
Siehe [Schritt 2.6](https://github.com/amzn/alexa-avs-raspberry-pi#26-install-java-development-kit)

Damit der Node.js Server richtig funktioniert muss auch NPM noch nachinstalliert werden.
Folgende Schritte sind dazu nötig:

```
#!python
sudo apt-get update 
sudo apt-get upgrade
curl -sL https://deb.nodesource.com/setup | sudo bash -
sudo apt-get install nodejs

```
Des Weiteren hat sich in der Anleitung ein kleiner Schreibfehler eingeschlichen.
Bei [Punkt 5](https://github.com/amzn/alexa-avs-raspberry-pi#5---install-the-dependencies) muss der Befehl 


```
#!python
npm instal 
```

Ebenfalls interessant ist der Python Fork namens [AlexiPi](https://github.com/sammachin/AlexaPi), welcher gerade dabei ist die Sprachunterstützung durch Hotword-Detection nachzurüsten.
siehe: [Snowboy](https://snowboy.kitt.ai/docs/)

Screeshot:
![1161661738-Screenshot AlexaPi.jpg](https://bitbucket.org/repo/gLkBzg/images/823421095-1161661738-Screenshot%20AlexaPi.jpg)