**[Zurück zu Home](Home)**

In dieser Wiki-Seite wird das Anwendungsprojekt "Alexa Sprachassisstent" und die Gruppe sowie das Vorgehen kurz dargestellt.

**Aufbau der Einleitung**  

[TOC]


## Vorstellung Anwendungsprojekt

Unser Anwendungsprojekt ist es, den Alexa Sprachassisstent von Amazon mittels Raspberry Pi nachzubauen - Services "quasi" ohne GUI. 
Ziel des Projektes ist es, die theoretischen Grundlagen aus der Vorlesung praktisch anzuwenden und zu vertiefen.
Mithilfe der Hardware, sollen verschiedene Services bereitgestellt werden, die am Ende des Hackathons als Demo vorgestellt werden.
Als Plattform der IoT-Services ist [IBM Bluemix](Theoretische Grundlagen) gesetzt 
![Einleitung.JPG](https://bitbucket.org/repo/gLkBzg/images/3398017839-Einleitung.JPG)

##Vorgehen & Gruppe

Das Projektteam besteht aus nachfolgenden Personen:

* [Martin Brümmer](https://www.facebook.com/martin.brummer.52)
* [Sascha Jörg](https://www.facebook.com/sascha.joerg1991)
* [Max Kienle](https://www.facebook.com/max.kienle)
* [Tobias Lindner](https://www.facebook.com/Lindner.T)
* [Sebastian Mantsch](https://www.facebook.com/basforever)
* [Johan Zhai](https://www.facebook.com/jo.han.5832343)


Aufgeteilt haben wir uns auf folgende Bereiche:

* ###Implementierung (Johan & Sebsatian)
![20160606_091415941_iOS.jpg](https://bitbucket.org/repo/gLkBzg/images/1025593518-20160606_091415941_iOS.jpg)

* ###Dokumentation (Max & Sascha)
![20160606_092352149_iOS.jpg](https://bitbucket.org/repo/gLkBzg/images/1730280833-20160606_092352149_iOS.jpg)

* ###Service-Entwurf (Martin & Tobias): Definition und Ausarbeitung der Services
![20160606_091434641_iOS.jpg](https://bitbucket.org/repo/gLkBzg/images/1611878127-20160606_091434641_iOS.jpg)



Wir wollen alle anfallenden Aufgaben in einer Art "Pair Programming" erledigen. Hierzu wurden Verantwortliche für die drei Hauptbereiche definiert. Je nach Arbeitsaufkommen werden die Aufgaben auf andere, nicht verantwortliche Teammitglieder übertragen. Vor dem Hackathon wird mehr Theoriearbeit verlangt und beim Hackathon wird der Fokus auf der Implementierung & der Dokumentation dieser liegen.

Für das Vorgehen hat sich das Team auf nachfolgende Punkte geeinigt. 
Diese werden im IoT Daily Report genauer dargestellt.

Das Team ist bei der Bearbeitung des Hackathons wie folgt vorgegangen:  
1. Unboxing Hardware  
2. Teamaufteilung & Verantwortlichkeiten für die einzelnen Bereiche Implementierung, Dokumentation und Service   
3. Einarbeitung in IBM Bluemix, Raspberry Pi + Alexa, Bitbucket  
4. Doing: Dokumentieren, Implementieren & Services entwerfen  
5. Präsentation vorbereiten: Demo finalisieren und Slides bearbeiten  
6. Präsentation und Abschluss   

##Daily Report

###24.05.2016 - Team- und Projektzielfindung###

Unboxing der notwendigen Hardware, Festlegen der benötigten Teile (Alle):

* Rasberry Pi 2 Model B
* XBOOM Lautsprecher
* Gold+ Memory SD Card
* Sienoc Mini Mikrofon

![IMG_6497.JPG](https://bitbucket.org/repo/gLkBzg/images/3225228348-IMG_6497.JPG)
 

Festlegen eines Projektziels (Alle):
Unterstützung des Lernens und der Lehre am HHZ, sowohl für Studierende als auch für Professoren
Es soll ein allumfassender HHZ Service mithilfe eines sprachgesteuerten Raspberry Pi umgesetzt werden. 
Siehe dazu auch [Brainstorming](Brainstorming)

### 06.06.2015 - Let's getting started###

Seit dem letzten Termin arbeiteten sich die einzelnen Mitglieder in die Themen rund um Sprachsteuerung und das Projektumfeld (Bitbucket, Raspberry Pi, etc.) ein. Hierfür wurde der Rasberry Pi soweit vorbereitet, dass der Alexa Sprachassistent darauf läuft. Näheres hierzu können Sie der Wikiseite [Experimentation](Experimentation) mit den Unterpunkten *Alexa lebt!* und *Raspberry Pi - Installation & Konfiguration* entnehmen.

Parallel hierzu wurde eine Bluemix Organisation aufgesetzt. Hierzu finden Sie weitere Infos auf der Wiki-Seite [Experimentation](Experimentation) mit den Unterpunkten *Bluemix*. Mit dieser haben sich alle Mitglieder auseinander gesetzt und erste praktischen Erfahrungen mit dem für alle neuen Tool gesammelt.

Um die gewonnen Erkenntnisse zu sammeln, wurde dieses Wiki in Bitbucket aufgesetzt. Hier möchten wir Ihnen unser erworbenes Wissen während der zwei Tage des Hackathons und auch die theoretischen Grundlagen aufzeigen. Für das Wiki wurde vorab eine Agenda aufgesetzt, damit von Beginn an eine Struktur sowohl für die Dokumentation als auch für das Projekt vorhanden war.

Um unsere Hackathon-Implementierungen in eine Richtung zu leiten, die den Studenten am HHZ echten Mehrwert liefert, wurde der Serviceentwurf dokumentiert. Dieses finden Sie unter der [Exploration-Phase](Exploration).

**Gestartet wurde am Montag um 9 Uhr und gegen 19:50 Uhr war der erste Tag des Hackathons beendet.
An dem Tag konnte der MVP fertiggestellt werden, die Service-Entwürfe waren finalisiert und die Dokumentation bereits sehr umfangreich ausgebaut.**

Obwohl bereits an dem Tag sehr viel Zeit investiert wurde und der Fortschritt sehr gut war, konnten sich ein(ige) Teammitglied(er) nicht von dem Hackathon losreißen und haben den Abend und die halbe Nacht noch für das Projekt drangehängt. Namen werden aber keine genannt!

![IMG_6501.JPG](https://bitbucket.org/repo/gLkBzg/images/1816489837-IMG_6501.JPG)

### 07.06.2015 - Finale###

Der Dienstag wurde (Beginn 9 Uhr) größtenteils damit genutzt, eine Qualitätssicherung der bestehenden Dokumentationen, MVP und Services durchzuführen.


![20160607_103628207_iOS.jpg](https://bitbucket.org/repo/gLkBzg/images/764619727-20160607_103628207_iOS.jpg)

Des weiteren wurden geprüft, dass alle Vorgaben des Hackathons erfüllt wurden und insbesondere die Verbindung zu Inhalten der Vorlesung noch herausgearbeitet (u.a. geschäftsrelevanter Bereich nach E.Fleisch, Value Proposition Canvas, UML-Diagramme, etc.)

Zudem wurde die Präsentation geplant und erstellt. Für die Präsentation und die Dokumentation wurde ein Video für die Vorstellung von Alexa erstellt.

![IMG_6795.JPG](https://bitbucket.org/repo/gLkBzg/images/4267126305-IMG_6795.JPG)

Da noch etwas Zeit übrig war, wurde ein weiterer Service implementiert (unabhängig vom MVP) der für die Präsentation ebenfalls verwendet wird. Hierfür haben Martin & Tobias die Truppe der "Implementierer" unterstützt. Links hierzu: [Weitere Services - bestimmen](Weitere Services - bestimmen) & [Weitere Services - implementieren](Weitere Services - implementieren)
