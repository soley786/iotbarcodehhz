**[Zurück zu Home](Home)**

# Unsichtbare Technologie - Steuerung mit natürlicher Sprache #
Die Nutzung von unsichtbarer Technologie stellt besondere Anprüche an IT Services. Insbesondere ist die Steuerung mit natürlicher Sprache nicht für alle Dienste geeignet.

Aus folgenden Gründen kann eine Servicesumsetzung mit Sprachein- und -ausgabe nicht immer möglich:

* Sicherheitsaspekte: Die Spracheingabe ist für Passwörter oder andere sicherheitskritische Informationen nicht sinnvoll.
* Umfangreiche Auswahlmöglichkeiten: Da der Nutzer die Auswahlmöglichkeiten lediglich hört, ist eine umfangsreiche Auswahliste nicht sinnvoll umsetzbar.
* Sachverhalte, die nicht mit eindeutigen Keywords beschrieben werden können, und dadurch ein gemeinsames Veständnis zwischen Maschine und Mensch erzeugen, können nicht sinnvoll umgesetzt werden. 
* Diagramme und Schaubilder können nicht sinnvoll und verständlich in Sprache umgesetzt werden.

Auf der [Amazon Alexa Produktseite](https://developer.amazon.com/public/solutions/alexa/alexa-skills-kit/docs/alexa-skills-kit-voice-design-best-practices) werden Best Practices zum Umgang mit Herausforderungen genannt. 
Im Folgenden wird auf die für dieses Projekt wichtigen Aspekte eingegangen:

* Ist eine Nachfrage beim Nutzer erforderlich oder wird eine weitere Auswahl benötigt, ist eine klare Herausstellung der Auswahlmöglichkeiten erforderlich: z.B. Essensbestellung: statt "Willst du Pommes oder Salat als Beilage?" besser "Welche Beilage möchtest du: Pommes oder Salat?"
* Klare, knappe und aussagekräftige Sätze, die nur die benötigten Informationen beinhalten?
* Bei der Aussgabe von Informationen in Sprache muss darauf geachtet werden, sich möglichst nahe am natürlichsprachlichen Aufbau von Sätzen orientiert.
* Wenn noch weitere Informationen benötigt werden, müssen diese Schritt-für-Schritt bei Nutzer nachgefragt werden.
* Nur notwendige Nachfragen implementieren: Der Nutzer muss nicht bei jeder Aktion, um eine Bestätigung gebeten werden. Nur bei Aktionen mit größeren Auswirkungen ist solch eine Nachfrage erforderlich: z.B. Social-Media Posts, Versenden einer Nachricht.
* Soll dem Nutzer eine längere Liste bereitgestellt werden, ist es von Vorteil diese in kleine Abschnitte zu zerteilen und dem Nutzer in mehreren Schritten zu überbringen. 
* Dem Nutzer muss ein klares Signal über Erfolg oder Misserfolg der Erkennung gegeben werden.

Die folgenden Darstellung wurden mit den hier hinterlegten Tools erzeugt: [eingesetzte Tools](Service Design - eingesetzte Tools)

# Lean Canvas #
![Lean-Canvas.JPG](https://bitbucket.org/repo/gLkBzg/images/477443367-Lean-Canvas.JPG)

# BPMN #
![BPMN_Alexa_V2.jpg](https://bitbucket.org/repo/gLkBzg/images/854622708-BPMN_Alexa_V2.jpg)

Das BPMN Modell stellt den Ablauf von der Alexa Ansprache bis zur Rückgabe der Antwort dar. Dadurch, dass im Bluemix unterschiedliche Services angesprochen werden, können unterschiedliche Funktionalitäten bereitgestellt werden. Der umgebende Prozessablauf bleibt dabei stets gleich.

# Sequenzdiagramm #
![Alexa_Sequenzdiagramm_V5.JPG](https://bitbucket.org/repo/gLkBzg/images/2230630971-Alexa_Sequenzdiagramm_V5.JPG)
